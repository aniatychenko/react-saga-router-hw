import {  FETCH_MESSAGES, EDIT_MESSAGE, ADD_MESSAGE, DELETE_MESSAGE, LIKE_MESSAGE} from "../actionTypes/messages";
import service from '../service';


export const fetchMessages = (messages) => ({
    type: FETCH_MESSAGES,
    payload: {
        messages
    }
})

// export const setUserId = (userId) => ({
//     type: SET_USER_ID,
//     payload: {
//         userId
//     }
// })
// export const setUserName = (user) => ({
//     type: SET_USER_NAME,
//     payload: {
//         user
//     }
// })

// export const changeText = (text) => ({
//     type: CHANGE_TEXT,
//     payload: {
//         text
//     }
// })


export const addMessage = (text, userId, userName) => ({
    type: ADD_MESSAGE,
    payload: {
        id: service.getNewId(),
        text,
        userId,
        user: userName,
        createdAt: new Date()
    }
});

export const editMessage = (id, data) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        data
    }
});


export const likeMessage = (id, isLiked) => ({
    type: LIKE_MESSAGE,
    payload: {
        id,
        isLiked
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});