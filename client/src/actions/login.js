import { LOGIN_USER } from "../actionTypes/login";

export const fetchUsersLogin = (data) => ({
    type: LOGIN_USER,
    payload: {
        data
    }
});