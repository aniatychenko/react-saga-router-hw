import { FETCH_MESSAGE } from "../actionTypes/modal";

export const fetchMessage = id => ({
    type: FETCH_MESSAGE,
    payload: {
        id
    }
});