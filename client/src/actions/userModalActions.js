import { FETCH_USER } from "../actionTypes/userModal";

export const fetchUser = id => ({
    type: FETCH_USER,
    payload: {
        id
    }
});