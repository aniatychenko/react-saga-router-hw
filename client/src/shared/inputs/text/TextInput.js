import React from 'react';
import PropTypes from 'prop-types';
import styles from '../../../components/EditUser/EditUser.module.scss'

const TextInput = ({ text, type, keyword, label, onChange }) => {
    const onChangeEvent = e => onChange(e, keyword);
    return (
        <div className={styles['form-group']}>
            <label >{ label }</label>
            <input
                value={ text }
                type={ type }
                onChange={ onChangeEvent }
            />
        </div>
    );
}

TextInput.propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
    keyword: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

export default TextInput;