import React, { useState }from 'react';
import PropTypes from 'prop-types';
import styles from '../../../components/EditUser/EditUser.module.scss'


const PasswordInput = ({ text, keyword, label, onChange }) => {
    const [isShown, setIsShown] = useState(false);
    const inputType = isShown ? 'text' : 'password';

    return (
        <div>
            <label className="">{ label }</label>
            <div className="form-group " style={{display: 'flex', flexDirection: "row"}}>
                <input
                    value={ text }
                    type={ inputType }
                    onChange={ e => onChange(e, keyword) }
                />
                <button style={{width: '2rem'}} onClick={() => setIsShown(!isShown) }>&#x1f441;</button>
            </div>
        </div>
    );
}

PasswordInput.propTypes = {
    text: PropTypes.string,
    keyword: PropTypes.string,
    label: PropTypes.string,
    onChange: PropTypes.func
};

export default PasswordInput;