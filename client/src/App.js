import './App.css';
import Chat from './components/Chat/Chat';
import logo from './images/logo.jpg';
import EditMessage from './components/EditMessage/EditMessage'
import Login from './components/Login/Login'
import UserList from './components/UserList/UserList'
import EditUser from './components/EditUser/EditUser'
import ProtectedRoure from './ProtectedRoute'
import { Switch, Route } from 'react-router-dom'


function App() {
  return (
    <div className="App">
      <header><img src={logo} alt="Logo" width="150px" /></header>
      <Switch>
        <Route exact path='/login' component={Login} />
        <Route exact path='/users' component={UserList} />
        <Route path='/user/:id' component={EditUser} />
        <Route path='/user/' component={EditUser} />
        <Route exact path='/messages' component={Chat} />
        <Route path='/messages/:id' component={EditMessage} />
        <ProtectedRoure path='/'/>
      </Switch>
      <footer>© Powered by Binary Studio Team</footer>

    </div>
  );
}

export default App;