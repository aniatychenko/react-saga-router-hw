import { FETCH_MESSAGE_SUCCESS } from "../actionTypes/modal";

const initialState = {
    messageData: {
        id: '',
        text: '',
        createdAt: '',
        user: '',
        userId: ''
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS: {
            const  messageData  = action.payload.messageData;
            return {
                ...state,
                messageData
            };
        }

        default:
            return state;
    }
}