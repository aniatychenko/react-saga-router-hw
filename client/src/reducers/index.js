import {combineReducers} from 'redux'
import messages from './messages'
import messageModal from './messageModal'
import users from './users'
import login from './login'
import userPage from './userPage'

const rootReducer = combineReducers({
    messages,
    messageModal,
    users,
    userPage,
    login
})

export default rootReducer