import { FETCH_MESSAGES_SUCCESS, SET_USER_ID, SET_USER_NAME, GET_MESSAGES, ADD_MESSAGE, CHANGE_TEXT, EDIT_MESSAGE, LIKE_MESSAGE, DELETE_MESSAGE } from "../actionTypes/messages";
const initialState = {
    messages: [],
    showLoader: true,
    currentUserId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce4"
}

// eslint-disable-next-line
export default function (state = initialState, action) {
    switch (action.type) {

        case FETCH_MESSAGES_SUCCESS: {
            const { messages } = action.payload
            return { ...state, messages, showLoader: action.payload.showLoader };

        }
        case LIKE_MESSAGE: {
            const { id, isLiked } = action.payload;
            const editMessages = state.messages.map(message => {
                if (message.id === id) {
                    return {
                        ...message,
                        isLiked: !isLiked
                    };
                } else {
                    return message;
                }
            });

            return {
                ...state,
                messages: editMessages
            };
        }

        default:
            return state;
    }
}
