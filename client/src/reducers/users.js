import { FETCH_USERS_SUCCESS } from "../actionTypes/users";

const initialState = {
    users: [],
    showLoader: true
}

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS_SUCCESS: {
            const { users } = action.payload
            return {...state, users, showLoader: action.payload.showLoader};
        }

        default:
            return state;
    }
}