import { FETCH_LOGIN_USER_SUCCESS } from "../actionTypes/login";

const initialState = {
    userData: {
        username: '',
        password: ''
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_LOGIN_USER_SUCCESS: {
            const  userData  = action.payload.user;
            return {
                ...state,
                userData,
                currentUserId: userData.id
            };
        }

        default:
            return state;
    }
}