import React from 'react'
import styles from './UserItem.module.scss'

const UserItem = (props) => {
    return (
        <div className={styles['user-item']}>
            <div className={styles['info-wrp']}>
                <p>{props.user.name + ' ' + props.user.surname + ' ' + props.user.username + ' ' + props.user.email}</p>
            </div>
            <div className={styles['btn-wrp']}>
                <button className={styles['edit-btn']} onClick={(ev) => props.edit(props.user.id)}>Edit</button>
                <button className={styles['delete-btn']} onClick={(ev) => props.delete(props.user.id)}>Delete</button>
            </div>
        </div>
    )
}

export default UserItem