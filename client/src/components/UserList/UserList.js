import React from 'react'
import styles from './UserList.module.scss'
import UserItem from '../UserItem/UserItem'
import * as actions from '../../actions/usersActions'
import { connect } from 'react-redux'
import Loading from '../Loading/Loading'
import { deleteUser } from '../../sagas/usersSaga'

class UserList extends React.Component {
  constructor(props) {
    super(props);
    // this.iterateUser = this.iterateUser.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onEdit = this.onEdit.bind(this);
  }


  iterateUser = (users) => {
    return users.map((u, index) => <UserItem key={index} user={u} delete={this.onDelete} edit={this.onEdit} />)
  }

  onEdit = (id) => {
    this.props.history.push(`/user/${id}`);
  }
  onAdd = () => {
    this.props.history.push('/user/');
  }

  onDelete = (id) => {
    this.props.deleteUser(id);
  }
  componentDidMount() {
    if (!this.props.userData.id) {
      this.props.history.push('/login/');
    } else {
      this.props.fetchUsers();
    }
  }
  render() {
    if (this.props.users.showLoader) {
      return <Loading />
    }

    return (
      <div className={styles['user-list-wrp']}>
        <button className={styles['add-btn']} onClick={(ev) => this.onAdd()}>Add User</button>
        {
          Object.values(this.props.users.users).map((u, i) => {
            return (
              <UserItem key={i} user={u} delete={this.onDelete} edit={this.onEdit} />
            )
          })
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
    ...state.login
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);