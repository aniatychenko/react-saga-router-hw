import React from 'react'
import styles from './DateComponent.module.scss'


const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

export default class DateComponent extends React.Component {
    render() {
        return (
            <div className={styles['date-block']}>
                <div className={styles['date']}>
                    {this.props.date.getDate()} {monthNames[this.props.date.getMonth()]}
                </div>
                <hr />
            </div>
        )
    }
}