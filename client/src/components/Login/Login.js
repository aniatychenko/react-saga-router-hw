import React from 'react'
import styles from './Login.module.scss'
import { connect } from 'react-redux'
import * as actions from '../../actions/login';
import defaultUserLoginConfig from '../../shared/config/defaultUserLoginConfig';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultUserData();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    getDefaultUserData() {
        return {
            ...defaultUserLoginConfig
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.currentUserId) {
            return {
                ...nextProps.userData
            };
        } else {
            return null;
        }
    }
    componentDidUpdate(){
        if(this.props.userData.role === 'admin')
        {
            this.props.history.push('/users');
        }
        else if (this.props.userData.role === 'user')
        {
            this.props.history.push('/messages');
        }

    }

    handleChange = (ev) => {
        const keyword = ev.target.name;

        const value = ev.target.value
        this.setState({[keyword]: value})
    }

    handleSubmit = () =>{
        this.props.fetchUsersLogin(this.state);
        // this.setState(this.getDefaultUserData());
        // this.props.history.push('/users')
    } 
    render() {
        return (
            <div className={styles['login-wrp']}>
                <form /*onSubmit={() => this.handleSubmit()}*/ className={styles['login']}>
                    <div className={styles['form-group']}>
                        <label> Username: </label>
                        <input name="username" type="text" className={styles['login-input']} value={this.state.username} onChange={(ev) => this.handleChange(ev)} />
                    </div>
                    <div className={styles['form-group']}>
                        <label> Password: </label>
                        <input name="password" type="password" className={styles['login-input']} value={this.state.password} onChange={(ev) => this.handleChange(ev)} />
                    </div>
                    <div className={styles['btn-group']}>
                        <input type="button" onClick={() => this.handleSubmit()} className={styles['submit-btn']} value="Log in" />
                    </div>
                </form>
            </div >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.login
    }
};

const mapDispatchToProps = {
    ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);