import React from 'react'
import styles from './MessageInput.module.scss'
import sendIcon from '../../images/arrow-circle-up-solid.svg'

export default class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.onChangeText = this.onChangeText.bind(this);
    }
    
    onChangeText = (text) => {
        this.setState(
            {
                ...this.state,
                text: text
            })
    }
    render() {
        return (
            <div className={styles['message-input']}>
                <textarea
                    placeholder="Message"
                    type="text"
                    onChange={(ev) => this.onChangeText(ev.target.value)}
                    className={styles['message-send-text']}
                    value={this.state.text || ''}>
                </textarea>
                <button
                    onClick={(ev) => {this.setState({...this.state, text: ''});this.props.send(this.state.text, this.props.currentUserId, this.props.currentUserName)}}
                    id={styles['send-button']}>
                    <img src={sendIcon} alt="send" width="32px" />Send</button>
            </div>
        )
    }
}