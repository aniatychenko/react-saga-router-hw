import React from 'react'
import styles from './EditMessage.module.scss'
import * as actions from '../../actions/messageModalActions';
import { editMessage } from '../../actions/messagesActions'
import defaultMessageConfig from '../../shared/config/defaultMessageConfig';
import { connect } from 'react-redux'

class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultMessageData();
        this.onCancel = this.onCancel.bind(this);
        this.onEdit = this.onEdit.bind(this);
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.messageData.id !== prevState.id && nextProps.match.params.id) {
            return {
                ...nextProps.messageData
            };
        } else {
            return null;
        }
    }
    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.fetchMessage(this.props.match.params.id)
        }
    }

    onCancel() {
        this.setState(this.getDefaultMessageData());
        this.props.history.push('/messages/');
    }

    onEdit() {
        this.props.editMessage(this.state.id, this.state);
        this.setState(this.getDefaultMessageData());
        this.props.history.push('/messages/');
    }

    onChangeData(e, keyword) {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                [keyword]: value
            }
        );
    }

    getDefaultMessageData() {
        return {
            ...defaultMessageConfig
        };
    }
    onChangeText(text) {
        this.setState(
            {
                ...this.state,
                text: text
            })
    }
    getModal() {
        return (
            <div className={styles['modal-background']}>
                <div className={styles['modal-block']}>
                    <textarea
                        onChange={(ev) => this.onChangeText(ev.target.value)}
                        className={styles['edit-textarea']}
                        value={this.state.text}>
                    </textarea>
                    <div className={styles['btn-wrp']}>
                        <button
                            className={styles['cancel-button']} onClick={this.onCancel}>Cancel</button>
                        <button
                            className={styles['edit-button']} onClick={this.onEdit}>Edit</button>
                    </div>
                </div>
            </div>
        )
    }
    render() {
        return this.getModal()
    }
}
const mapStateToProps = (state) => {
    return {
        messageData: state.messageModal.messageData
    }
};

const mapDispatchToProps = {
    ...actions,
    editMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);