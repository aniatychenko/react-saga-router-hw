import React from 'react'
import styles from './Header.module.scss'

const Header = function (props) {
    const participants = new Set();
    const participantsId = new Set(); 
    props.messageItemsInfo.sort(function (a, b) {
        return new Date(a.createdAt) - new Date(b.createdAt);
    })
    props.messageItemsInfo.forEach(message => {
                    participantsId.add(message.userId);
                    participants.add(message.user);
                })
    const messagesAmount = props.messageItemsInfo.length
    const lastMessageTime = messagesAmount ? new Date(props.messageItemsInfo[messagesAmount - 1].createdAt) : null;
    const lastMessage = lastMessageTime.getHours() + ":" + lastMessageTime.getMinutes()

        return (
            <div className={styles['chat-header']}>
                <p>My Chat</p>
                <p>{participants.size} members</p>
                <p>{messagesAmount} messages</p>
                <p>Last message at: {lastMessage}</p>
            </div>
        )
}

export default Header