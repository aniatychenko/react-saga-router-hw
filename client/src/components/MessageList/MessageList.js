import React from 'react'
import styles from './MessageList.module.scss'
import MessageItem from '../MessageItem/MessageItem'
import DateComponent from '../DateComponent/DateComponent'


export default class MessageList extends React.Component {
    render() {
        let currDate = new Date();
        this.props.messageItemsInfo.sort(function (a, b) {
                        return new Date(a.createdAt) - new Date(b.createdAt);
                    })
        return (
            <div className={styles['chat-list']}>
                {   
                    Object.values(this.props.messageItemsInfo).map((val, i) => {
                        const date = new Date(val.createdAt)
                        const time = (new Date(val.createdAt)).getHours() + ':' + (new Date(val.createdAt)).getMinutes()
                        if (currDate.getDate() !== date.getDate()) {
                            currDate = date;
                            return (
                                <div key={val.id}>
                                    <DateComponent key={i} date={date} />
                                    <MessageItem
                                        key={val.id}
                                        delete={this.props.delete}
                                        editIcon={this.props.editIcon}
                                        messageId={val.id}
                                        name={val.user}
                                        text={val.text}
                                        avatar={val.avatar}
                                        userId={val.userId}
                                        time={time}
                                        editedAt={val.editedAt}
                                        currUser={this.props.currUser} />
                                </div>
                            )
                        }
                        return (
                            <MessageItem
                                key={val.id}
                                messageId={val.id}
                                name={val.user}
                                text={val.text}
                                delete={this.props.delete}
                                editIcon={this.props.editIcon}
                                avatar={val.avatar}
                                userId={val.userId}
                                isLiked={val.isLiked}
                                time={time}
                                like={this.props.like}
                                currUser={this.props.currUser}
                            />
                        )
                    }
                    )}
            </div>
        )
    }
}