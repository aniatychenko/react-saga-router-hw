import React from 'react'
import styles from './Chat.module.scss'
import Header from '../Header/Header'
import MessageList from '../MessageList/MessageList'
import MessageInput from '../MessageInput/MessageInput'
import * as actions from '../../actions/messagesActions'
import { connect } from 'react-redux'
import Loading from '../Loading/Loading'

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.onSend = this.onSend.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onEditIcon = this.onEditIcon.bind(this);
        this.onLike = this.onLike.bind(this);

    }
    componentDidMount() {
        if(!this.props.userData.id){
            this.props.history.push('/login/');
        }
        else{
            this.props.fetchMessages();
        }
    }
    componentWillUnmount() {
    }
    onLike = (id, isLiked) => {
        this.props.likeMessage(id, isLiked)
    }
    onEditIcon = (messageId) => {
        this.props.history.push(`/messages/${messageId}`)
    }
    onDelete = (id) => {
        this.props.deleteMessage(id)
    }
    onChangeText = (text) => {
        this.props.changeText(text)
    }
    onSend = (text, userId, user) => {

        this.props.addMessage(text, userId, user)
    }

    render() {
        if (this.props.showLoader) {
            return <Loading />;
        }
        return (
            <div className={styles['chat-wrp']}>
                <Header
                    messageItemsInfo={this.props.messages}
                />
                <MessageList
                     messageItemsInfo={this.props.messages}
                     currUser={this.props.userData.id}
                     like={this.onLike}
                     delete={this.onDelete}
                     editIcon={this.onEditIcon}
                />
                <MessageInput
                    currentUserId={this.props.userData.id}
                    currentUserName={this.props.userData.name}
                    text={this.props.text}
                    send={this.onSend} />
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        ...state.messages,
        ...state.messageModal,
        ...state.login
    }
};

const mapDispatchToProps = {
    ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);