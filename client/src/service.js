const getNewId = () => (new Date()).getTime().toString();

// eslint-disable-next-line
export default {
    getNewId
};