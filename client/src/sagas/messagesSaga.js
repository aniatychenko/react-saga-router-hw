import axios from 'axios';
import api from '../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, FETCH_MESSAGES } from "../actionTypes/messages";

export function* fetchMessages() {
	try {
        const messages = yield call(axios.get, `${api.url}/messages`);
		yield put({ type: 'FETCH_MESSAGES_SUCCESS', payload: { messages: messages.data, showLoader: false } })
	} catch (error) {
		console.log('fetchMessages error:', error.message)
	}
}

function* watchFetchMessages() {
	yield takeEvery(FETCH_MESSAGES, fetchMessages)
}


export function* addMessage(action) {
	const newUser = { ...action.payload};
	try {
		yield call(axios.post, `${api.url}/messages`, newUser);
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
		console.log('createMessage error:', error.message);
	}
}

function* watchAddMessage() {
	yield takeEvery(ADD_MESSAGE, addMessage)
}

export function* editMessage(action) {
	const id = action.payload.id;
    const updatedMessage = { ...action.payload.data };
	
	try {
		yield call(axios.put, `${api.url}/messages/${id}`, updatedMessage);
		yield put({ type: FETCH_MESSAGES });
	} catch (error) {
		console.log('updateMessage error:', error.message);
	}
}

function* watchUpdateMessage() {
	yield takeEvery(EDIT_MESSAGE, editMessage)
}

export function* deleteMessage(action) {
	try {
		yield call(axios.delete, `${api.url}/messages/${action.payload.id}`);
		yield put({ type: FETCH_MESSAGES })
	} catch (error) {
		console.log('deleteMessage Error:', error.message);
	}
}

function* watchDeleteMessage() {
	yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* usersSagas() {
	yield all([
		watchFetchMessages(),
		watchAddMessage(),
		watchUpdateMessage(),
		watchDeleteMessage()
	])
};