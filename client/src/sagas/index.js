import { all } from 'redux-saga/effects';
import userPageSagas from './userPageSaga';
import usersSagas from './usersSaga';
import messagePageSagas from './messagePage';
import messagesSagas from './messagesSaga';

export default function* rootSaga() {
    yield all([
        userPageSagas(),
        usersSagas(),
        messagesSagas(),
        messagePageSagas()
    ])
};