const userRoutes = require('./userRoutes');
const usersRoutes = require('./usersRoutes');
const authRoutes = require('./authRoutes');
const messageRoutes = require('./messageRoutes');
const fightRoutes = require('./fightRoutes');

module.exports = (app) => {
    app.use('/users', usersRoutes);
    app.use('/user', userRoutes);
    app.use('/messages', messageRoutes);
    app.use('/fights', fightRoutes);
    app.use('/login', authRoutes);
  };