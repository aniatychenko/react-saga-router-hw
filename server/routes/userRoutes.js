const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();


router.get('/', (req, res, next) => {
    try {
        const data = UserService.getAll();
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const userId = req.params.id;
        const data = UserService.findById(userId);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', (req, res, next) => {
    try {
        const data = UserService.create(req.body);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
    try {
        const userId = req.params.id;
        const data = UserService.update(userId, req.body);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const userId = req.params.id;
        const data = UserService.delete(userId);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;