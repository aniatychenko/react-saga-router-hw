const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', (req, res, next) => {
    try {
        const data = AuthService.login({username: req.body.username, password: req.body.password});
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;