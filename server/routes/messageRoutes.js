const { Router } = require('express');
const MessageService = require('../services/messageService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        const data = MessageService.getAll()
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: "hhh",
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        const messageId = req.params.id;
        const data = MessageService.search(messageId);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.post('/', (req, res, next) => {
    try {
        const data = MessageService.create(req.body);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.put('/:id', (req, res, next) => {
    try {
        const messageId = req.params.id;
        const data = MessageService.update(messageId, req.body);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        const messageId = req.params.id;
        const data = MessageService.delete(messageId);
        res.responseData = {
            data,
            status: 200
        };
    } catch (err) {
        res.err = {
            status: 404,
            message: err.message,
        };
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;