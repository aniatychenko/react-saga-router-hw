const { UserRepository } = require('../repositories/userRepository');

class UserService {

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw new Error('User not found');
        }
        return item;
    }
    findById(id) {
        const item = UserRepository.getOneById(id);
        if(!item) {
            throw new Error('User not found');
        }
        return item;
    }
    getAll() {
        const items = UserRepository.getAll();
        return items;
    }
    create(data) {
        data.role = 'user';
        const item = UserRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }
    update(id, newData) {
        const item = UserRepository.update(id, newData);
        return item;
    }
    delete(id) {
        const item = UserRepository.delete(id);
        if(!item.length) {
            throw new Error('User not found');
        }
        return item;
    }
}

module.exports = new UserService();