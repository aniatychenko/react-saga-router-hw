const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    // TODO: Implement methods to work with fighters
    search(id) {
        const item = MessageRepository.getOneById(id);
        if(!item) {
            throw new Error('Fighter not found');
        }
        return item;
    }
    getAll() {
        const items = MessageRepository.getAll();
        if(!items)
        {
            return null;
        }
        return items;
    }
    create(data) {
        const item = MessageRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }
    update(id, newData) {
        const item = MessageRepository.update(id, newData);
        if(!item) {
            return null;
        }
        return item;
    }
    delete(id) {
        const item = MessageRepository.delete(id);
        if(!item.length) {
            throw new Error('Fighter not found');
        }
        return item;
    }
}

module.exports = new MessageService();