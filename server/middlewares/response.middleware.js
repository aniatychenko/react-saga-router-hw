const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   if(res.err){
        const {message, status} = res.err;
        res.status(status).send({
            error: true,
            message: message || 'Some error occured'
        });
    }
    else {
        const {data, status} = res.responseData; 
        res.status(status || 200).send(data);
    }
}

exports.responseMiddleware = responseMiddleware;