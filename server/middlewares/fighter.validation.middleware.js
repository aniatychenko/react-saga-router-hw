const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {

    req.body.health = req.body.health || 100;
    if(hasEmptyField(req.body)) {
        res.status(400).send({
            error: true,
            message: 'Fighter entity has empty field'
        })
    }
    else if(hasNotNumberFields(req.body)) {
        res.status(400).send({
            error: true,
            message: 'Field types error'
        })
    }
    else if(req.body.power >= 100 || req.body.power <= 0) {
        res.status(400).send({
            error: true,
           message: 'Power must be in the range from 1 to 100'
        })
    }
    else if(req.body.defense < 1 || req.body.defense > 10) {
        res.status(400).send({
            error: true,
            message: 'Defense must be in the range from 1 to 10'
        })
    }
    else if(Boolean(req.body.id)) {
        res.status(400).send({
            error: true,
            message: 'Id must not be stored in the body'
        })
    }
    else if(hasOddFields(req.body, fighter))
    {
        res.status(400).send({
            error: true,
            message: 'Request body has odd fields'
        })
    }
    else
    {
        next();
    }
}

const updateFighterValid = (req, res, next) => {

    req.body.health = req.body.health || 100;
    if(hasNotNumberFields(req.body)) {
        res.status(400).send({
            error: true,
            message: 'Field types error'
        })
    }
    else if(req.body.power && (req.body.power >= 100 || req.body.power <= 0)) {
        res.status(400).send({
            error: true,
            message: 'Power must be in the range from 1 to 100'
        })
    }
    else if(req.body.defense && (req.body.defense < 1 || req.body.defense > 10)) {
        res.status(400).send({
            error: true,
            message: 'Defense must be in the range from 1 to 10'
        })
    }
    else if(Boolean(req.body.id)) {
        res.status(400).send({
            error: true,
            message: 'Id must not be stored in the body'
        })
    }
    else if(hasOddFields(req.body, fighter))
    {
        res.status(400).send({
            error: true,
            message: 'Request body has odd fields'
        })
    }
    else
    {
        next();
    }
}

const hasEmptyField = ({name, power, defense}) => {
    return !name || !power || !defense;
}
const hasOddFields = (body, model) => {
    return !Object.keys(body).every(bodyKey => Object.keys(model).includes(bodyKey));
}
const hasNotNumberFields = ({power = 0, defense = 0, health = 0}) => {
    return isNaN(power) || isNaN(defense) || isNaN(health)
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;