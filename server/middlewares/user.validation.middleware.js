const { user } = require('../models/user');

const reEmail = /^[\w]{1}[\w-\.]*@gmail\.com$/
const rePhone = /^\+380\d{9}$/

const createUserValid = (req, res, next) => {
    if(hasEmptyField(req.body)) {
        res.status(400).send({
            error: true,
            message: 'User entity has empty field'
        })
    }
    else if(req.body.password.length < 3) {
        res.status(400).send({
            error: true, 
            message: 'Password is too short'
        })
    }
    else if(!reEmail.test(req.body.email))
    {
        res.status(400).send({
            error: true,
            message: 'Email must be valid gmail format'
        })
    }
    else if(Boolean(req.body.id)) {
        res.status(400).send({
            error: true,
            message: 'Id must not be stored in the body'
        })
    }
    else if(!rePhone.test(req.body.phoneNumber))
    {
        res.status(400).send({
            error: true,
            message: 'Phone number must be in format +380xxxxxxxxx'
        })
    }
    else if(hasOddFields(req.body, user))
    {
        res.status(400).send({
            error: true,
            message: 'Request body has odd fields'
        })
    }
    else
    {
        next();
    }
}

const updateUserValid = (req, res, next) => {

    if(req.body.password && req.body.password.length < 3) {
        res.status(400).send({
            error: true, 
            message: 'Password is too short'
        })
    }
    else if(req.body.email && !reEmail.test(req.body.email))
    {
        res.status(400).send({
            error: true,
            message: 'Email must be valid gmail format'
        })
    }
    else if(Boolean(req.body.id)) {
        res.status(400).send({
            error: true,
            message: 'Id must not be stored in the body'
        })
    }
    else if(req.body.phoneNumber && !rePhone.test(req.body.phoneNumber))
    {
        res.status(400).send({
            error: true,
            message: 'Phone number must be in format +380xxxxxxxxx'
        })
    }
    else if(hasOddFields(req.body, user))
    {
        res.status(400).send({
            error: true,
            message: 'Request body has odd fields'
        })
    }
    else
    {
        next();
    }
}
const hasEmptyField = ({firstName, lastName, email, phoneNumber, password}) => {
    return !firstName || !lastName || !email || !phoneNumber || !password
}
const hasOddFields = (body, model) => {
    return !Object.keys(body).every(bodyKey => Object.keys(model).includes(bodyKey));
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;